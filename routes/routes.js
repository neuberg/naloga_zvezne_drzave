const userRoutes = require("./mesta");

const appRouter = (app, fs) => {
  // we've added in a default route here that handles empty routes
  // at the base API url
  app.get("/poskus", (req, res) => {});

  userRoutes(app, fs);
};

module.exports = appRouter;
