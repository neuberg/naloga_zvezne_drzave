function naloziMesta() {
  const getNewCases = async () => {
    const response = await fetch("http://localhost:3000/mesta", {
      method: "GET",
      mode: "cors",
    });
    const data = await response.json();

    $("#loadingLabel").hide();
    $("#tabelaMest").DataTable({
      data: data,
      bLengthChange: true,
      columns: [
        { data: "city", title: "city" },
        { data: "growth_from_2000_to_2013", title: "growth from 2000 to 2013" },
        { data: "latitude", title: "latitude" },
        { data: "longitude", title: "longitude" },
        { data: "population", title: "population" },
        { data: "rank", title: "rank" },
        { data: "state", title: "state" },
      ],
    });
  };

  scrollToPager = () => {
    var y = $(window).scrollTop();
    $("html, body").animate({
      scrollTop: y + $("#tabelaMest").height(),
    });
  };

  getNewCases();
}
